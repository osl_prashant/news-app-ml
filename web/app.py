import re
import spacy

from flask import Flask, jsonify, request, abort
from flask_restful import Api, Resource
from gensim.summarization import summarize
from gensim.summarization import keywords
# Some doc string has been directly refrenced from gensim library

from newspaper import Article
from transformers import pipeline


#import transformers
app = Flask(__name__)
api = Api(app)

# set device equal to 0 or greater for GPU
# defined global as loading is large and time taking
summarizer = pipeline(
    "summarization", model='facebook/bart-large-cnn', device=-1)  # t5-large
print("summarizer loaded")


# spacy keywords nlp
nlp = spacy.load('en_core_web_md')


def get_the_summary(news, split=True, ratio=0.2, word_count=None):
    """
    news : str
        Given news.
    ratio : float
        Number between 0 and 1 that determines the proportion of the number of
        sentences of the original text to be chosen for the summary.
    word_count : int or None,
        Determines how many words will the output contain.
        If both parameters are provided, the ratio will be ignored.
    split : bool
        If True, list of sentences will be returned. Otherwise joined
        strings will be returned.


    Returns
    ------------
    list of str
        If `split` **OR**
    """
    df_summary = []
    try:
        df_summary.append(summarize(news, split=split,
                                    ratio=ratio, word_count=word_count))
        print(df_summary[-1])
    except:
        df_summary.append(
            'Check if paramaters makes sense or Lines are very Few')

    return df_summary


def get_the_keyowrds_TextRank(news, split=True, ratio=None, scores=True, keyword_count=15):
    """
    news : str
        Input news.
    ratio : float
        If no "words" option is selected, the number of sentences is reduced by the provided ratio,
        else, the ratio is ignored.
    keyword_count : int
        Number of returned keywords.
    split : bool
        Whether split keywords if True.
    scores : bool
        Whether return score of keyword.

    Returns
    ------------------------------------
    result: list of (str, float)
        If `scores`, keywords with scores 
        **OR**
    result: list of str
    """
    df_keywords = []
    try:
        df_keywords.append(keywords(news, split=split, ratio=ratio, scores=scores,
                                    words=keyword_count,  lemmatize=True, pos_filter=('NN', 'JJ')))
    except:
        df_keywords.append(
            ['Check if paramaters makes sense or Lines are very Few'])

    return df_keywords


def get_the_keywords_spacy(sentence):
    """
    news : str
        Input news.
    """

    sentence = sentence.lower()
    doc = nlp(sentence)
    keywords_list = set()
    for ent in doc.ents:
        keywords_list.add((ent.text, ent.label_))

    keywords_list = list(keywords_list)
    keywords_freq_list = []
    total = 0
    for keys in keywords_list:
        freq = len(re.findall(re.escape(keys[0]), sentence))
        freq = freq*freq  # square to emphasize the repeated words more
        total += freq
        keywords_freq_list.append([keys[0], keys[1], freq])

    for i, keys in enumerate(keywords_freq_list):
        keywords_freq_list[i][2] /= total

    keywords_freq_list.sort(key=lambda x: float(x[2]), reverse=True)

    return keywords_freq_list


class Home(Resource):
    def get(self):
        retJSON = {
            'message': "Hello, Working",
            'Status Code': 200,
        }

        return jsonify(retJSON)


class Keywords_TextRank(Resource):
    def post(self):
        # write what to do for post request and Add class
        # Load the data
        postedData = request.get_json()

        # Validate the data
        # news
        news = postedData['news']

        # split
        if not (postedData['split'] == 'True' or postedData['split'] == 'False'):
            return abort(400, "split is Bool Datatype. Input 'True' or 'False'.")
        split = bool(postedData['split'])

        # ratio
        ratio = postedData['ratio']

        if ratio == "None":
            ratio = None
        else:
            if not isinstance(ratio, float):
                return abort(400, "ratio is float Datatype. Allowed range is (0,1).")
            elif ratio >= 1.0 or ratio <= 0.0:
                return abort(400, "ratio is float Datatype. Allowed range is (0,1).")

        # scores
        if not (postedData['scores'] == 'True' or postedData['scores'] == 'False'):
            return abort(400, "scores is `Bool` Datatype. Input 'True' or 'False'.")
        scores = bool(postedData['scores'])

        # keyword_count
        keyword_count = postedData['keyword_count']
        if keyword_count == "None":
            keyword_count = None
        else:
            if not isinstance(keyword_count, int):
                return abort(400, "keyword_count is `int` Datatype. Number of keyword_count should be less than total words")

        # generate keywords
        keywords_gensim = get_the_keyowrds_TextRank(
            news, split=split, scores=scores, ratio=ratio, keyword_count=keyword_count)

        # make json and return
        retJSON = {
            'keywords_gensim': keywords_gensim,
            'Status Code': 200,
        }

        return jsonify(retJSON)


class Keywords_Spacy(Resource):
    def post(self):
        # write what to do for post request and Add class
        # Load the data
        postedData = request.get_json()

        # Validate the data
        news = postedData['news']

        # generate keywords
        keywords_spacy = get_the_keywords_spacy(news)

        # make json and return
        retJSON = {
            'keywords_spacy': keywords_spacy,
            'Status Code': 200,
        }

        return jsonify(retJSON)


class Primary_Summary(Resource):
    def post(self):
        # write what to do for post request and Add class
        # Load the data
        postedData = request.get_json()

        # Validate the data

        news = postedData['news']

        # Model at once can only take 1024 tokens (tokens -> words + special charachters)
        # Only choosing about 690 words.
        news = ' '.join(news.split(' ')[:500])

        min_len = postedData['min_len']
        max_len = postedData['max_len']

        if (not isinstance(min_len, int)) or (not isinstance(max_len, int)):
            return abort(400, "Max and Min lengths need to be integer. ")

        primary_summary = summarizer(
            news, min_length=min_len, max_length=max_len)
        summary_word_count = len(primary_summary[0]['summary_text'].split())

        # make json and return
        retJSON = {
            'Primary_Summary': primary_summary[0]['summary_text'],
            'word_count': summary_word_count,
            'Status Code': 200,
        }

        return jsonify(retJSON)


class Secondary_Summary(Resource):
    def post(self):
        # write what to do for post request and Add class
        # Load the data
        postedData = request.get_json()

        # Validate the data

        news = postedData['news']

        # split
        if not (postedData['split'] == 'True' or postedData['split'] == 'False'):
            return abort(400, "split is Bool Datatype. Input 'True' or 'False'.")
        split = bool(postedData['split'])

        # ratio
        ratio = postedData['ratio']

        if ratio == "None":
            ratio = None
        else:
            if not isinstance(ratio, float):
                return abort(400, "ratio is float Datatype. Allowed range is (0,1).")
            elif ratio >= 1.0 or ratio <= 0.0:
                return abort(400, "ratio is float Datatype. Allowed range is (0,1).")

        # word_count
        word_count = postedData['word_count']
        if word_count == "None":
            word_count = None
        else:
            if not isinstance(word_count, int):
                return abort(400, "word_count is `int` Datatype. Number of word_count should be less than total words")

        secondary_summary = get_the_summary(
            news, split=split, ratio=ratio, word_count=word_count)

        # make json and return
        retJSON = {
            'Secondary_Summary': secondary_summary,
            'Status Code': 200,
        }

        return jsonify(retJSON)


class Scrap_URL(Resource):

    def post(self):
        postedData = request.get_json()

        url = postedData['url']

        url = str(url)

        try:
            article = Article(url)
            article.download()
            article.parse()
        except:
            return abort(400, "Error in parsing, check `url`")

        try:
            authors = article.authors
            time = article.publish_date
            content = article.text
            img_src = article.top_image
        except:
            return abort(400, "Error in extraction from Article.")

        retJSON = {
            'content': content,
            'authors': authors,
            'time': time,
            'img_src': img_src,
            'Status Code': 200
        }

        return jsonify(retJSON)


api.add_resource(Primary_Summary, '/primary_summary')
api.add_resource(Secondary_Summary, '/secondary_summary')
api.add_resource(Keywords_TextRank, '/keywords_textrank')
api.add_resource(Keywords_Spacy, '/keywords_spacy')
api.add_resource(Home, '/home')
api.add_resource(Scrap_URL, '/scrap_url')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)  # host='0.0.0.0'
